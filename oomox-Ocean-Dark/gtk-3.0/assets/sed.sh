#!/bin/sh
sed -i \
         -e 's/#343d46/rgb(0%,0%,0%)/g' \
         -e 's/#dfe1e8/rgb(100%,100%,100%)/g' \
    -e 's/#2b303b/rgb(50%,0%,0%)/g' \
     -e 's/#bf616a/rgb(0%,50%,0%)/g' \
     -e 's/#4f5b66/rgb(50%,0%,50%)/g' \
     -e 's/#eff1f5/rgb(0%,0%,50%)/g' \
	*.svg
